'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.actions = exports.initialState = undefined;
exports.reducers = reducers;

var _axios = require('axios');

var _axios2 = _interopRequireDefault(_axios);

var _isArray = require('lodash/isArray');

var _isArray2 = _interopRequireDefault(_isArray);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var initialState = exports.initialState = {
  data: [],
  total: 0,
  meta: {},
  error: null,
  last: null
};

function reducers(key) {
  var state = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  var action = arguments[2];

  switch (action.type) {
    case key + '/GRID/LOADING':
      return Object.assign({}, state, {
        grid: Object.assign({}, state.grid, {
          loading: action.loading
        })
      });
    case key + '/GRID/DATA':
      if (action.appendData) {
        action.data.data = state.grid.data.concat(action.data.data);
      }
      return Object.assign({}, state, {
        grid: Object.assign({}, initialState, action.data)
      });
    default:
      return state;
  }
}

var actions = exports.actions = {
  fetch: function fetch(key, endpoint, _ref) {
    var _ref$filters = _ref.filters,
        filters = _ref$filters === undefined ? {} : _ref$filters,
        _ref$sorting = _ref.sorting,
        sorting = _ref$sorting === undefined ? {} : _ref$sorting,
        _ref$limit = _ref.limit,
        limit = _ref$limit === undefined ? 10 : _ref$limit,
        _ref$offset = _ref.offset,
        offset = _ref$offset === undefined ? 0 : _ref$offset;
    var appendData = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;

    return function (dispatch) {
      dispatch({ type: key + '/GRID/LOADING', key: key, loading: true });
      (0, _axios2.default)({
        method: 'post',
        url: endpoint,
        data: {
          where: filters, sort: sorting, offset: offset, limit: limit
        }
      }).then(function (response) {
        var actionData = {
          last: {
            filters: filters, sorting: sorting, limit: limit, offset: offset
          }
        };
        if ((0, _isArray2.default)(response.data)) {
          actionData.data = response.data;
        } else {
          var _response$data = response.data,
              data = _response$data.data,
              meta = _response$data.meta,
              total = _response$data.total;

          actionData.data = data;
          actionData.total = parseInt(total);
          if (meta) {
            actionData.meta = meta;
          }
        }
        dispatch({ type: key + '/GRID/DATA', key: key, data: actionData, appendData: appendData });
        dispatch({ type: key + '/GRID/LOADING', key: key, loading: false });
      }).catch(function (error) {
        console.error(error);
        if (error.response) {
          dispatch({
            type: key + '/GRID/DATA',
            key: key,
            data: {
              error: error.response.data
            }
          });
        }
        dispatch({ type: key + '/GRID/LOADING', key: key, loading: false });
      });
    };
  }
};