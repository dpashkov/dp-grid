'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _isArray = require('lodash/isArray');

var _isArray2 = _interopRequireDefault(_isArray);

var _isFunction = require('lodash/isFunction');

var _isFunction2 = _interopRequireDefault(_isFunction);

var _isEmpty = require('lodash/isEmpty');

var _isEmpty2 = _interopRequireDefault(_isEmpty);

var _isEqual = require('lodash/isEqual');

var _isEqual2 = _interopRequireDefault(_isEqual);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _icon = require('./icon');

var _icon2 = _interopRequireDefault(_icon);

var _blockui = require('./blockui');

var _blockui2 = _interopRequireDefault(_blockui);

var _pagination = require('./pagination');

var _pagination2 = _interopRequireDefault(_pagination);

var _search = require('./icons/search.svg');

var _search2 = _interopRequireDefault(_search);

var _clear = require('./icons/clear1.svg');

var _clear2 = _interopRequireDefault(_clear);

var _refresh = require('./icons/refresh.svg');

var _refresh2 = _interopRequireDefault(_refresh);

var _repeat = require('./icons/repeat.svg');

var _repeat2 = _interopRequireDefault(_repeat);

var _sort = require('./icons/sort.svg');

var _sort2 = _interopRequireDefault(_sort);

var _sortAsc = require('./icons/sort-asc.svg');

var _sortAsc2 = _interopRequireDefault(_sortAsc);

var _sortDesc = require('./icons/sort-desc.svg');

var _sortDesc2 = _interopRequireDefault(_sortDesc);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
// import { StickyContainer, Sticky } from 'react-sticky'

// ICONS // by fontawesome.io


var Grid = function (_React$Component) {
  _inherits(Grid, _React$Component);

  function Grid(props) {
    _classCallCheck(this, Grid);

    var _this = _possibleConstructorReturn(this, (Grid.__proto__ || Object.getPrototypeOf(Grid)).call(this, props));

    _this.state = {
      limit: 10,
      filters: {},
      offset: 0,
      sorting: [],
      selectedRows: {}
    };

    _this.handleLimitChange = _this.handleLimitChange.bind(_this);
    _this.handleChangeFilter = _this.handleChangeFilter.bind(_this);
    _this.handleApplyFilters = _this.handleApplyFilters.bind(_this);
    return _this;
  }

  _createClass(Grid, [{
    key: 'isStaticData',
    value: function isStaticData() {
      return !this.props.fetch;
    }

    /**
     * set selectedRows to all (de)selected and update UI accordingly
     * @param {Boolean} checked   check status of the all selected checkbox
     */

  }, {
    key: 'selectAll',
    value: function selectAll(checked) {
      var _getGridData = this.getGridData(),
          data = _getGridData.data;

      var selectedRows = {};
      if ((0, _isArray2.default)(data) && data.length) {
        for (var i = 0; i < data.length; i++) {
          selectedRows[i] = true;
        }
      }
      this.setState({ selectedRows: selectedRows });
    }

    /**
     * respond to a user (de)selecting a single row
     * @param {Int} i             the index of the row that was clicked
     * @param {Boolean} selected   the new check status of that row
     */

  }, {
    key: 'setRowSelected',
    value: function setRowSelected(i, selected) {
      var selectedRows = this.state.selectedRows;
      if (selected) {
        selectedRows[i] = true;
      } else if (selectedRows[i]) {
        delete selectedRows[i];
      }
      this.setState({
        selectedRows: Object.assign({}, selectedRows)
      });
    }
  }, {
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(newProps) {
      var _this2 = this;

      if (!(0, _isEqual2.default)(newProps.filters, this.props.filters)) {
        var filters = {};
        if (newProps.filters) {
          newProps.filters.forEach(function (f) {
            if (f.value !== undefined) {
              filters[f.key] = f.value;
            }
          });
        }
        this.setState({
          filters: Object.assign({}, this.state.filters, filters)
        }, function () {
          return _this2.reload();
        });
      }
    }
  }, {
    key: 'componentDidMount',
    value: function componentDidMount() {
      var _this3 = this;

      var savedState = localStorage.getItem('grid.' + this.props.id);
      if (savedState) {
        savedState = JSON.parse(savedState);
        if (savedState.limit) {
          this.handleLimitChange(parseInt(savedState.limit), false);
        }
        if (savedState.filters) {
          this.setState({
            filters: Object.assign({}, savedState.filters, this.state.filters)
          }, function () {
            _this3.reload();
          });
        } else {
          this.reload();
        }
      } else {
        this.reload();
      }
    }
  }, {
    key: 'getGridData',
    value: function getGridData() {
      return this.props.data;
    }
  }, {
    key: 'saveToLocal',
    value: function saveToLocal() {
      var props = {
        limit: this.state.limit,
        filters: this.state.filters
      };
      localStorage.setItem('grid.' + this.props.id, JSON.stringify(props));
    }
  }, {
    key: 'handleLimitChange',
    value: function handleLimitChange(limit) {
      var _this4 = this;

      var reload = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

      if (this.state.limit !== limit) {
        this.setState({ limit: limit }, function () {
          _this4.saveToLocal();
          if (reload && _this4.getGridData().offset === 0) {
            _this4.reload();
          }
        });
      }
    }
  }, {
    key: 'reload',
    value: function reload() {
      this.saveToLocal();
      if (this.props.fetch) {
        var _state = this.state,
            offset = _state.offset,
            limit = _state.limit,
            filters = _state.filters,
            sorting = _state.sorting;

        this.props.fetch({
          offset: offset, limit: limit, filters: filters, sorting: sorting
        });
        this.setState({ selectedRows: [] });
      }
    }
  }, {
    key: 'loadMore',
    value: function loadMore() {
      var _this5 = this;

      if (this.props.fetch) {
        var _state2 = this.state,
            offset = _state2.offset,
            limit = _state2.limit,
            filters = _state2.filters,
            sorting = _state2.sorting;

        var newOffset = parseInt(limit) + parseInt(offset);
        this.setState({
          offset: newOffset,
          selectedRows: []
        }, function () {
          _this5.props.fetch({
            offset: newOffset, limit: limit, filters: filters, sorting: sorting
          }, true);
        });
      }
    }
  }, {
    key: 'loadPage',
    value: function loadPage(page) {
      var _this6 = this;

      this.setState({
        offset: this.state.limit * page
      }, function () {
        return _this6.reload();
      });
    }
  }, {
    key: 'changeSort',
    value: function changeSort(column) {
      var _this7 = this;

      var direction = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'ASC';

      var sort = {};
      sort[column] = direction;
      this.setState({ sorting: [sort] }, function () {
        _this7.reload();
      });
    }
  }, {
    key: 'handleChangeFilter',
    value: function handleChangeFilter(event) {
      var filters = this.state.filters;
      filters[event.target.name] = event.target.value;
      this.setState({ filters: filters });
    }
  }, {
    key: 'handleApplyFilters',
    value: function handleApplyFilters(event) {
      if (event.key === 'Enter') {
        this.reload();
      }
    }
  }, {
    key: 'clearFilters',
    value: function clearFilters() {
      var _this8 = this;

      var reload = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

      this.setState({
        sorting: [],
        filters: {}
      }, function () {
        _this8.reload();
      });
    }
  }, {
    key: 'sum',
    value: function sum(column) {
      var _this9 = this;

      var _getGridData2 = this.getGridData(),
          data = _getGridData2.data;

      if (data && data.length) {
        return data.reduce(function (sum, row, index) {
          if (!(0, _isEmpty2.default)(_this9.state.selectedRows)) {
            if (_this9.state.selectedRows[index]) {
              return sum + parseFloat(row[column]);
            }
          } else {
            return sum + parseFloat(row[column]);
          }
          return sum;
        }, 0).toFixed(2);
      }
      return 0;
    }
  }, {
    key: 'renderPagination',
    value: function renderPagination() {
      var _this10 = this;

      var _getGridData3 = this.getGridData(),
          total = _getGridData3.total;

      var _state3 = this.state,
          limit = _state3.limit,
          offset = _state3.offset;

      return _react2.default.createElement(_pagination2.default, { total: total, limit: limit, offset: offset, loadPage: function loadPage(p) {
          return _this10.loadPage(p);
        } });
    }
  }, {
    key: 'renderFilters',
    value: function renderFilters() {
      var _this11 = this;

      var dynamicFilters = this.getGridData().meta && this.getGridData().meta.filters;
      var filters = this.props.filters;

      if (filters && filters.length) {
        return _react2.default.createElement(
          'div',
          { className: (0, _classnames2.default)('search-box', this.props.classes && this.props.classes.filters) },
          _react2.default.createElement(
            'div',
            { className: 'form-inline' },
            filters.map(function (f, index) {
              var renderFilterItem = function renderFilterItem() {
                switch (f.type) {
                  case 'number':
                    return _react2.default.createElement('input', {
                      type: 'number',
                      name: f.key,
                      className: 'form-control',
                      placeholder: f.placeholder,
                      onKeyPress: _this11.handleApplyFilters,
                      onChange: _this11.handleChangeFilter,
                      value: _this11.state.filters[f.key] || ''
                    });
                  case 'select':
                    var options = f.options || [];
                    if ((!options || options.length === 0) && dynamicFilters && dynamicFilters[f.key] && (0, _isArray2.default)(dynamicFilters[f.key])) {
                      options = dynamicFilters[f.key];
                    }

                    return _react2.default.createElement(
                      'select',
                      {
                        name: f.key,
                        className: 'form-control',
                        onKeyPress: _this11.handleApplyFilters,
                        onChange: _this11.handleChangeFilter,
                        value: _this11.state.filters[f.key] || ''
                      },
                      f.placeholder && _react2.default.createElement(
                        'option',
                        { value: '' },
                        f.placeholder
                      ),
                      options.map(function (o, index) {
                        return _react2.default.createElement(
                          'option',
                          { key: index, value: o.value },
                          o.title
                        );
                      })
                    );
                  case 'string':
                  case 'text':
                  default:
                    return _react2.default.createElement('input', {
                      type: 'text',
                      name: f.key,
                      className: 'form-control',
                      placeholder: f.placeholder,
                      onKeyPress: _this11.handleApplyFilters,
                      onChange: _this11.handleChangeFilter,
                      value: _this11.state.filters[f.key] || ''
                    });
                }
              };

              var style = {};
              if (f.width) {
                style.width = f.width;
              }

              return _react2.default.createElement(
                'div',
                { key: index, className: 'form-group', style: style },
                renderFilterItem()
              );
            }),
            this.props.customs && this.props.customs.search,
            _react2.default.createElement(
              'button',
              { className: 'btn btn-light ml-3', onClick: function onClick() {
                  return _this11.clearFilters(true);
                } },
              _react2.default.createElement(_icon2.default, { icon: _clear2.default, size: '1.2em', style: { margin: '-3px 0 -1px 0' } })
            ),
            !this.isStaticData() && _react2.default.createElement(
              'button',
              { className: 'btn btn-light', onClick: function onClick() {
                  return _this11.reload();
                } },
              _react2.default.createElement(_icon2.default, { icon: _refresh2.default })
            ),
            _react2.default.createElement(
              'button',
              { className: 'btn btn-light ml-3', onClick: function onClick() {
                  return _this11.reload();
                } },
              _react2.default.createElement(_icon2.default, { icon: _search2.default }),
              ' Search'
            )
          )
        );
      }
      return null;
    }
  }, {
    key: 'render',
    value: function render() {
      var _this12 = this;

      var _props = this.props,
          id = _props.id,
          columns = _props.columns;

      var _getGridData4 = this.getGridData(),
          data = _getGridData4.data,
          loading = _getGridData4.loading,
          total = _getGridData4.total,
          error = _getGridData4.error;

      var _state4 = this.state,
          selectedRows = _state4.selectedRows,
          limit = _state4.limit,
          offset = _state4.offset,
          sorting = _state4.sorting;


      var dataLength = (0, _isArray2.default)(data) && data.length || 0;
      var allRowsSelected = dataLength === selectedRows.length && dataLength !== 0;

      var sortKeys = {};
      if (sorting) {
        sorting.forEach(function (k) {
          var column = Object.keys(k)[0];
          sortKeys[column] = k[column];
        });
      }

      return _react2.default.createElement(
        'div',
        { className: 'grid-table', id: 'grid_table_' + id },
        error && _react2.default.createElement(
          'div',
          { className: 'alert alert-danger mb-5' },
          error
        ),
        this.renderFilters(),
        _react2.default.createElement(
          _blockui2.default,
          { tag: 'div', blocking: !!loading },
          _react2.default.createElement(
            'div',
            null,
            _react2.default.createElement(
              'div',
              { className: 'table-responsive' },
              _react2.default.createElement(
                'table',
                { className: (0, _classnames2.default)('table', this.props.classes && this.props.classes.table || 'table-grid table-hover') },
                _react2.default.createElement(
                  'thead',
                  null,
                  _react2.default.createElement(
                    'tr',
                    null,
                    columns.map(function (col, index) {
                      var style = {};
                      if (col.width) style['width'] = col.width;

                      var text = null;
                      if (col.sort) {
                        var sortDirection = 'ASC';
                        var SortIcon = _sort2.default;

                        if (sortKeys[col.key]) {
                          SortIcon = sortKeys[col.key] === 'ASC' ? _sortAsc2.default : _sortDesc2.default;
                          sortDirection = sortKeys[col.key] === 'ASC' ? 'DESC' : 'ASC';
                        }

                        text = _react2.default.createElement(
                          'span',
                          {
                            className: 'table-col-title pointer',
                            onClick: function onClick() {
                              return _this12.changeSort(col.key, sortDirection);
                            }
                          },
                          _react2.default.createElement(
                            'span',
                            { className: (0, _classnames2.default)('sorting-icon', { 'active': sortKeys[col.key] }) },
                            _react2.default.createElement(_icon2.default, { icon: SortIcon, size: '0.9em' })
                          ),
                          col.header
                        );
                      } else {
                        text = _react2.default.createElement(
                          'span',
                          { className: 'table-col-title' },
                          col.header
                        );
                      }

                      var sum = null;
                      if (col.sum) {
                        sum = _react2.default.createElement(
                          'div',
                          { className: 'sum' },
                          _react2.default.createElement(
                            'b',
                            null,
                            '\u2211'
                          ),
                          ' ',
                          _this12.sum(col.key)
                        );
                      }

                      return _react2.default.createElement(
                        'th',
                        { key: index, style: style },
                        _react2.default.createElement(
                          'div',
                          { className: 'table-col-header' },
                          text,
                          sum
                        )
                      );
                    })
                  )
                ),
                _react2.default.createElement(
                  'tbody',
                  null,
                  (0, _isEmpty2.default)(data) ? this.renderEmptyTable() : data.map(function (row, i) {
                    return _react2.default.createElement(
                      'tr',
                      { className: (0, _classnames2.default)({ 'selected': selectedRows[i] }), onClick: function onClick() {
                          return _this12.setRowSelected(i, !selectedRows[i]);
                        }, key: i },
                      columns.map(function (col, j) {
                        var value = null;
                        if (col.cell && (0, _isFunction2.default)(col.cell)) {
                          value = col.cell(row);
                        } else if (col.key) {
                          if (!value) value = row[col.key];
                        }
                        var cellClass = col.cellClass && (0, _isFunction2.default)(col.cellClass) ? col.cellClass(row) : null;
                        return _react2.default.createElement(
                          'td',
                          { key: j, className: cellClass },
                          value
                        );
                      })
                    );
                  })
                )
              )
            ),
            Array.isArray(data) && !this.isStaticData() && _react2.default.createElement(
              'div',
              { className: this.props.classes && this.props.classes.pagination },
              _react2.default.createElement(
                'div',
                { className: 'grid-pagination row' },
                _react2.default.createElement(
                  'div',
                  { className: 'col-total col-md-3' },
                  _react2.default.createElement(
                    'button',
                    { disabled: offset + limit >= total, className: 'btn btn-light btn-sm pl-3 pr-3',
                      onClick: function onClick() {
                        return _this12.loadMore();
                      } },
                    _react2.default.createElement(_icon2.default, { icon: _repeat2.default }),
                    ' Load more'
                  ),
                  _react2.default.createElement(
                    'small',
                    { className: 'text-muted ml-2' },
                    _react2.default.createElement(
                      'b',
                      null,
                      data.length,
                      ' of ',
                      total
                    ),
                    ' records loaded'
                  )
                ),
                total > data.length && _react2.default.createElement(
                  'div',
                  { className: 'col-pagination col-md-6' },
                  _react2.default.createElement(
                    'ul',
                    { className: 'pagination' },
                    this.renderPagination()
                  )
                ),
                _react2.default.createElement(
                  'div',
                  { className: 'col-perpage col-md-3' },
                  _react2.default.createElement(
                    'select',
                    {
                      className: 'form-control per-page',
                      onChange: function onChange(e) {
                        return _this12.handleLimitChange(e.target.value);
                      },
                      value: limit
                    },
                    [10, 20, 50, 100, 200, 500].map(function (pageSize) {
                      return _react2.default.createElement(
                        'option',
                        {
                          className: pageSize === limit ? 'font-weight-bold' : '',
                          key: pageSize,
                          value: pageSize
                        },
                        pageSize,
                        ' per page'
                      );
                    })
                  )
                )
              )
            )
          )
        )
      );
    }
  }, {
    key: 'renderEmptyTable',
    value: function renderEmptyTable() {
      return _react2.default.createElement(
        'tr',
        null,
        _react2.default.createElement(
          'td',
          { colSpan: this.props.columns.length, className: 'text-center p-2' },
          'No data found'
        )
      );
    }
  }]);

  return Grid;
}(_react2.default.Component);

;

Grid.propTypes = {
  id: _propTypes2.default.string.isRequired,
  data: _propTypes2.default.shape({
    data: _propTypes2.default.array,
    total: _propTypes2.default.number,
    loading: _propTypes2.default.bool,
    meta: _propTypes2.default.object // help information
  }),
  fetch: _propTypes2.default.func,
  columns: _propTypes2.default.arrayOf(_propTypes2.default.shape({
    header: _propTypes2.default.node.isRequired,
    key: _propTypes2.default.string.isRequired,
    sort: _propTypes2.default.bool,
    sum: _propTypes2.default.bool,
    cell: _propTypes2.default.func,
    hide: _propTypes2.default.bool,
    rowClass: _propTypes2.default.func,
    cellClass: _propTypes2.default.func
  })).isRequired,
  filters: _propTypes2.default.arrayOf(_propTypes2.default.shape({
    key: _propTypes2.default.string.isRequired,
    placeholder: _propTypes2.default.string.isRequired,
    options: _propTypes2.default.array,
    type: _propTypes2.default.string,
    value: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number])
  })),
  classes: _propTypes2.default.shape({
    table: _propTypes2.default.string,
    filters: _propTypes2.default.string,
    pagination: _propTypes2.default.string
  }),
  customs: _propTypes2.default.shape({
    search: _propTypes2.default.node
  })
};

exports.default = Grid;