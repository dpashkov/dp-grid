'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = Pagination;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _icon = require('./icon');

var _icon2 = _interopRequireDefault(_icon);

var _longArrowLeft = require('./icons/long-arrow-left.svg');

var _longArrowLeft2 = _interopRequireDefault(_longArrowLeft);

var _longArrowRight = require('./icons/long-arrow-right.svg');

var _longArrowRight2 = _interopRequireDefault(_longArrowRight);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// ICONS
function Pagination(_ref) {
  var total = _ref.total,
      limit = _ref.limit,
      offset = _ref.offset,
      loadPage = _ref.loadPage,
      _ref$links = _ref.links,
      links = _ref$links === undefined ? 3 : _ref$links;

  var currentPage = Math.ceil(offset / limit);
  var last = Math.ceil(total / limit) - 1;
  var start = currentPage - links > 0 ? currentPage - links : 0;
  var end = currentPage + links < last ? currentPage + links : last;

  var pagination = [];

  pagination.push(_react2.default.createElement(
    'li',
    { key: 'prev', className: 'page-item ' + (currentPage === 0 ? 'disabled' : '') },
    _react2.default.createElement(
      'a',
      { className: 'page-link', onClick: function onClick() {
          return currentPage > 0 ? loadPage(currentPage - 1) : null;
        } },
      _react2.default.createElement(_icon2.default, { icon: _longArrowLeft2.default })
    )
  ));

  if (start > 0) {
    pagination.push(_react2.default.createElement(
      'li',
      { key: 'first', className: 'page-item' },
      _react2.default.createElement(
        'a',
        { className: 'page-link', onClick: function onClick() {
            return loadPage(0);
          } },
        1
      )
    ));
    if (start !== 1) {
      pagination.push(_react2.default.createElement(
        'li',
        { key: 'dotsFirst', className: 'page-item disabled' },
        _react2.default.createElement(
          'a',
          { className: 'page-link' },
          '...'
        )
      ));
    }
  }

  var _loop = function _loop(i) {
    pagination.push(_react2.default.createElement(
      'li',
      { key: i, className: 'page-item ' + (currentPage === i ? 'active' : '') },
      _react2.default.createElement(
        'a',
        { className: 'page-link', onClick: function onClick() {
            return loadPage(i);
          } },
        i + 1
      )
    ));
  };

  for (var i = start; i <= end; i++) {
    _loop(i);
  }

  if (end < last) {
    if (end + 1 !== last) {
      pagination.push(_react2.default.createElement(
        'li',
        { key: 'dotsLast', className: 'page-item disabled' },
        _react2.default.createElement(
          'a',
          { className: 'page-link' },
          '...'
        )
      ));
    }
    pagination.push(_react2.default.createElement(
      'li',
      { key: 'last', className: 'page-item' },
      _react2.default.createElement(
        'a',
        { className: 'page-link', onClick: function onClick() {
            return loadPage(last);
          } },
        last + 1
      )
    ));
  }

  pagination.push(_react2.default.createElement(
    'li',
    { key: 'next', className: 'page-item ' + (currentPage === last ? 'disabled' : '') },
    _react2.default.createElement(
      'a',
      { className: 'page-link', onClick: function onClick() {
          return currentPage === last ? null : loadPage(currentPage + 1);
        } },
      _react2.default.createElement(_icon2.default, { icon: _longArrowRight2.default })
    )
  ));

  return pagination;
}