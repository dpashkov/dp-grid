'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = Icon;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function Icon(_ref) {
  var className = _ref.className,
      _ref$style = _ref.style,
      style = _ref$style === undefined ? {} : _ref$style,
      _ref$size = _ref.size,
      size = _ref$size === undefined ? '1em' : _ref$size,
      icon = _ref.icon;

  if (size) {
    style.width = size;
  }
  if (!icon) return null;
  if (!icon.id) {
    return _react2.default.createElement(
      'span',
      { className: (0, _classnames2.default)('svg-icon', className), style: style },
      _react2.default.createElement('img', { src: icon, alt: '', style: { width: size } })
    );
  }
  return _react2.default.createElement(
    'span',
    { className: (0, _classnames2.default)('svg-icon', className), style: style },
    _react2.default.createElement(
      'svg',
      { viewBox: icon.viewBox },
      _react2.default.createElement('use', { xlinkHref: '#' + icon.id })
    )
  );
}