const gulp = require('gulp');
const babel = require('gulp-babel');
const del = require('del');
const bump = require('gulp-bump');

process.env.NODE_ENV = 'production';
 
gulp.task('default', ['build', 'bump']);

gulp.task('build', ['clean'], () => {
  gulp.src([ 'src/**/*.svg', 'src/*.scss' ])
    .pipe(gulp.dest('dist'));

  gulp.src('src/*.js')
    .pipe(babel())
    .pipe(gulp.dest('dist'));
});

gulp.task('bump', function () {
  gulp.src('./*.json')
    .pipe(bump({type: 'patch'}))
    .pipe(gulp.dest('./'));
});

gulp.task('clean', function () {
  return del('dist/**', {force: true});
});

gulp.task('js', () =>
  gulp.src([ 'src/**/*.svg', 'src/*.scss' ])
    .pipe(gulp.dest('dist'))
);

gulp.task('files', () => 
  gulp.src('src/*.js')
    .pipe(babel())
    .pipe(gulp.dest('dist'))
);
