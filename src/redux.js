import axios from 'axios'
import isArray from 'lodash/isArray'

export const initialState = {
  data: [],
  total: 0,
  meta: {},
  error: null,
  last: null
};

export function reducers (key, state = {}, action) {
  switch (action.type) {
    case `${key}/GRID/LOADING`:
      return {
        ...state,
        grid: {
          ...state.grid,
          loading: action.loading
        }
      }
    case `${key}/GRID/DATA`:
      if (action.appendData) {
        action.data.data = state.grid.data.concat(action.data.data)
      }
      return {
        ...state,
        grid: {
          ...initialState,
          ...action.data
        }
      }
    default:
      return state
  }
}

export const actions = {
  fetch: function (key, endpoint, { filters = {}, sorting = {}, limit = 10, offset = 0 }, appendData = false) {
    return (dispatch) => {
      dispatch({ type: `${key}/GRID/LOADING`, key, loading: true })
      axios({
        method: 'post',
        url: endpoint,
        data: {
          where: filters, sort: sorting, offset: offset, limit: limit
        }
      })
        .then((response) => {
          const actionData = {
            last: {
              filters, sorting, limit, offset
            }
          };
          if (isArray(response.data)) {
            actionData.data = response.data;
          } else {
            const { data, meta, total } = response.data;
            actionData.data = data;
            actionData.total = parseInt(total);
            if (meta) {
              actionData.meta = meta;
            }
          }
          dispatch({ type: `${key}/GRID/DATA`, key, data: actionData, appendData })
          dispatch({ type: `${key}/GRID/LOADING`, key, loading: false })
        })
        .catch(error => {
          console.error(error)
          if (error.response) {
            dispatch({
              type: `${key}/GRID/DATA`,
              key,
              data: {
                error: error.response.data
              }
            })
          }
          dispatch({ type: `${key}/GRID/LOADING`, key, loading: false })
        })
    }
  }
}
