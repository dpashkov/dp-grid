import React from 'react'
import classNames from 'classnames'

export default function Icon ({ className, style = {}, size = '1em', icon }) {
  if (size) {
    style.width = size
  }
  if (!icon) return null
  if (!icon.id) {
    return (
      <span className={classNames('svg-icon', className)} style={style}>
        <img src={icon} alt="" style={{width: size}}/>
      </span>
    )
  }
  return (
    <span className={classNames('svg-icon', className)} style={style}>
      <svg viewBox={icon.viewBox}>
        <use xlinkHref={`#${icon.id}`} />
      </svg>
    </span>
  )
}
