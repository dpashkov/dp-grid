import React from 'react'
import Icon from './icon'

// ICONS
import FaLongArrowLeft from './icons/long-arrow-left.svg'
import FaLongArrowRight from './icons/long-arrow-right.svg'

export default function Pagination ({ total, limit, offset, loadPage, links = 3 }) {
  const currentPage = Math.ceil(offset / limit)
  const last = Math.ceil(total / limit) - 1
  const start = ((currentPage - links) > 0) ? currentPage - links : 0
  const end = ((currentPage + links) < last) ? currentPage + links : last

  const pagination = []

  pagination.push(
    <li key='prev' className={`page-item ${(currentPage === 0) ? 'disabled' : ''}`}>
      <a className='page-link' onClick={() => currentPage > 0 ? loadPage(currentPage - 1) : null }>
        <Icon icon={FaLongArrowLeft}/>
      </a>
    </li>
  )

  if (start > 0) {
    pagination.push(
      <li key='first' className={`page-item`}>
        <a className='page-link' onClick={() => loadPage(0)}>
          {1}
        </a>
      </li>
    )
    if (start !== 1) {
      pagination.push(
        <li key='dotsFirst' className='page-item disabled'>
          <a className='page-link'>...</a>
        </li>
      )
    }
  }

  for (let i = start; i <= end; i++) {
    pagination.push(
      <li key={i} className={`page-item ${(currentPage === i) ? 'active' : ''}`}>
        <a className='page-link' onClick={() => loadPage(i)}>
          {i + 1}
        </a>
      </li>
    )
  }

  if (end < last) {
    if (end + 1 !== last) {
      pagination.push(
        <li key='dotsLast' className='page-item disabled'>
          <a className='page-link'>...</a>
        </li>
      )
    }
    pagination.push(
      <li key='last' className={`page-item`}>
        <a className='page-link' onClick={() => loadPage(last)}>
          {last + 1}
        </a>
      </li>
    )
  }

  pagination.push(
    <li key='next' className={`page-item ${(currentPage === last) ? 'disabled' : ''}`}>
      <a className='page-link' onClick={() => (currentPage === last) ? null : loadPage(currentPage + 1)}>
        <Icon icon={FaLongArrowRight}/>
      </a>
    </li>
  )

  return pagination
}
