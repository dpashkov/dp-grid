import React from 'react'
import PropTypes from 'prop-types'
import isArray from 'lodash/isArray'
import isFunction from 'lodash/isFunction'
import isEmpty from 'lodash/isEmpty'
import isEqual from 'lodash/isEqual'
import classNames from 'classnames'
// import { StickyContainer, Sticky } from 'react-sticky'

import Icon from './icon'
import BlockUi from './blockui'
import Pagination from './pagination'

// ICONS // by fontawesome.io
import FaSearch from './icons/search.svg'
import FaClear from './icons/clear1.svg'
import FaReload from './icons/refresh.svg'
import FaRepeat from './icons/repeat.svg'
import FaSort from './icons/sort.svg'
import FaSortAsc from './icons/sort-asc.svg'
import FaSortDesc from './icons/sort-desc.svg'

class Grid extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      limit: 10,
      filters: {},
      offset: 0,
      sorting: [],
      selectedRows: {}
    };

    this.handleLimitChange = this.handleLimitChange.bind(this)
    this.handleChangeFilter = this.handleChangeFilter.bind(this)
    this.handleApplyFilters = this.handleApplyFilters.bind(this)
  }

  isStaticData () {
    return !this.props.fetch
  }

  /**
   * set selectedRows to all (de)selected and update UI accordingly
   * @param {Boolean} checked   check status of the all selected checkbox
   */
  selectAll (checked) {
    const { data } = this.getGridData();
    const selectedRows = {}
    if (isArray(data) && data.length) {
      for (let i = 0; i < data.length; i++) {
        selectedRows[i] = true
      }
    }
    this.setState({ selectedRows })
  }

  /**
   * respond to a user (de)selecting a single row
   * @param {Int} i             the index of the row that was clicked
   * @param {Boolean} selected   the new check status of that row
   */
  setRowSelected (i, selected) {
    const selectedRows = this.state.selectedRows
    if (selected) {
      selectedRows[i] = true
    } else if (selectedRows[i]) {
      delete selectedRows[i]
    }
    this.setState({
      selectedRows: {
        ...selectedRows
      }
    })
  }

  componentWillReceiveProps (newProps) {
    if (!isEqual(newProps.filters, this.props.filters)) {
      const filters = {};
      if (newProps.filters) {
        newProps.filters.forEach(f => {
          if (f.value !== undefined) {
            filters[f.key] = f.value;
          }
        });
      }
      this.setState({
        filters: {
          ...this.state.filters,
          ...filters
        }
      }, () => this.reload())
    }
  }

  componentDidMount () {
    let savedState = localStorage.getItem(`grid.${this.props.id}`);
    if (savedState) {
      savedState = JSON.parse(savedState);
      if (savedState.limit) {
        this.handleLimitChange(parseInt(savedState.limit), false)
      }
      if (savedState.filters) {
        this.setState({
          filters: {
            ...savedState.filters,
            ...this.state.filters
          }
        }, () => {
          this.reload()
        })
      } else {
        this.reload()
      }
    } else {
      this.reload()
    }    
  }

  getGridData () {
    return this.props.data
  }

  saveToLocal () {
    const props = {
      limit: this.state.limit,
      filters: this.state.filters
    };
    localStorage.setItem(`grid.${this.props.id}`, JSON.stringify(props));
  }

  handleLimitChange (limit, reload = true) {
    if (this.state.limit !== limit) {
      this.setState({ limit }, () => {
        this.saveToLocal();
        if (reload && this.getGridData().offset === 0) {
          this.reload()
        }
      });
    }
  }

  reload () {
    this.saveToLocal();
    if (this.props.fetch) {
      const { offset, limit, filters, sorting } = this.state;
      this.props.fetch({
        offset, limit, filters, sorting
      })
      this.setState({ selectedRows: [] })
    }
  }

  loadMore () {
    if (this.props.fetch) {
      const { offset, limit, filters, sorting } = this.state;
      const newOffset = parseInt(limit) + parseInt(offset)
      this.setState({
        offset: newOffset,
        selectedRows: []
      }, () => {
        this.props.fetch({
          offset: newOffset, limit, filters, sorting
        }, true)
      });
    }
  }

  loadPage (page) {
    this.setState({
      offset: this.state.limit * page
    }, () => this.reload())
  }

  changeSort (column, direction = 'ASC') {
    const sort = {}
    sort[column] = direction
    this.setState({ sorting: [sort] }, () => {
      this.reload()
    });
  }

  handleChangeFilter (event) {
    const filters = this.state.filters
    filters[event.target.name] = event.target.value
    this.setState({ filters })
  }

  handleApplyFilters (event) {
    if (event.key === 'Enter') {
      this.reload()
    }
  }

  clearFilters (reload = false) {
    this.setState({
      sorting: [],
      filters: {},
    }, () => {
      this.reload()
    })
  }

  sum (column) {
    const { data } = this.getGridData();
    if (data && data.length) {
      return data.reduce((sum, row, index) => {
        if (!isEmpty(this.state.selectedRows)) {
          if (this.state.selectedRows[index]) {
            return sum + parseFloat(row[column]);
          }
        } else {
          return sum + parseFloat(row[column]);
        }
        return sum;
      }, 0).toFixed(2)
    }
    return 0
  }

  renderPagination () {
    const { total } = this.getGridData();
    const { limit, offset } = this.state;
    return <Pagination total={total} limit={limit} offset={offset} loadPage={p => this.loadPage(p)} />
  }

  renderFilters () {
    const dynamicFilters = this.getGridData().meta && this.getGridData().meta.filters;
    const { filters } = this.props;
    if (filters && filters.length) {
      return (
        <div className={classNames('search-box', this.props.classes && this.props.classes.filters)}>
          <div className='form-inline'>
            {filters.map((f, index) => {
              const renderFilterItem = () => {
                switch (f.type) {
                  case 'number':
                    return (
                      <input
                        type='number'
                        name={f.key}
                        className='form-control'
                        placeholder={f.placeholder}
                        onKeyPress={this.handleApplyFilters}
                        onChange={this.handleChangeFilter}
                        value={this.state.filters[f.key] || ''}
                      />
                    )
                  case 'select':
                    let options = f.options || [];
                    if ((!options || options.length === 0) && dynamicFilters && dynamicFilters[f.key] && isArray(dynamicFilters[f.key])) {
                      options = dynamicFilters[f.key]
                    }

                    return (
                      <select
                        name={f.key}
                        className='form-control'
                        onKeyPress={this.handleApplyFilters}
                        onChange={this.handleChangeFilter}
                        value={this.state.filters[f.key] || ''}
                      >
                        {f.placeholder && <option value=''>{f.placeholder}</option>}
                        {options.map((o, index) => {
                          return (
                            <option key={index} value={o.value}>{o.title}</option>
                          )
                        })}
                      </select>
                    )
                  case 'string':
                  case 'text':
                  default:
                    return (
                      <input
                        type='text'
                        name={f.key}
                        className='form-control'
                        placeholder={f.placeholder}
                        onKeyPress={this.handleApplyFilters}
                        onChange={this.handleChangeFilter}
                        value={this.state.filters[f.key] || ''}
                      />
                    )
                }
              };

              const style = {}
              if (f.width) {
                style.width = f.width
              }

              return (
                <div key={index} className='form-group' style={style}>
                  {renderFilterItem()}
                </div>
              )
            })}
            {this.props.customs && this.props.customs.search}
            <button className='btn btn-light ml-3' onClick={() => this.clearFilters(true)}>
              <Icon icon={FaClear} size="1.2em" style={{margin: '-3px 0 -1px 0'}}/>
            </button>
            {!this.isStaticData() && (
              <button className="btn btn-light" onClick={() => this.reload()}>
                <Icon icon={FaReload}/>
              </button>
            )}
            <button className='btn btn-light ml-3' onClick={() => this.reload()}>
              <Icon icon={FaSearch}/> Search
            </button>
          </div>
        </div>
      )
    }
    return null
  }

  render () {
    const { id, columns } = this.props
    const { data, loading, total, error } = this.getGridData()
    const { selectedRows, limit, offset, sorting } = this.state

    const dataLength = (isArray(data) && data.length) || 0
    const allRowsSelected = dataLength === selectedRows.length && dataLength !== 0

    const sortKeys = {}
    if (sorting) {
      sorting.forEach(k => {
        const column = Object.keys(k)[0]
        sortKeys[column] = k[column]
      })
    }

    return (
      <div className='grid-table' id={`grid_table_${id}`}>
        {error && (
          <div className="alert alert-danger mb-5">
            {error}
          </div>
        )}
        {this.renderFilters()}
        <BlockUi tag='div' blocking={!!loading}>
          <div>
            <div className='table-responsive'>
              <table className={classNames('table', (this.props.classes && this.props.classes.table) || 'table-grid table-hover')}>
                <thead>
                  <tr>
                    {columns.map((col, index) => {
                      const style = {}
                      if (col.width) style['width'] = col.width

                      let text = null
                      if (col.sort) {
                        let sortDirection = 'ASC'
                        let SortIcon = FaSort

                        if (sortKeys[col.key]) {
                          SortIcon = sortKeys[col.key] === 'ASC' ? FaSortAsc : FaSortDesc
                          sortDirection = sortKeys[col.key] === 'ASC' ? 'DESC' : 'ASC'
                        }

                        text = (
                          <span
                            className="table-col-title pointer"
                            onClick={() => this.changeSort(col.key, sortDirection)}
                          >
                            <span className={classNames('sorting-icon', {'active': sortKeys[col.key]})}>
                              <Icon icon={SortIcon} size="0.9em"/>
                            </span>
                            {col.header}
                          </span>
                        )
                      } else {
                        text = <span className="table-col-title">{col.header}</span>
                      }

                      let sum = null
                      if (col.sum) {
                        sum = <div className='sum'><b>&sum;</b> {this.sum(col.key)}</div>
                      }

                      return (
                        <th key={index} style={style}>
                          <div className="table-col-header">
                            {text}
                            {sum}
                          </div>
                        </th>
                      )
                    })}
                  </tr>
                </thead>
                <tbody>
                  {isEmpty(data) ? this.renderEmptyTable() : data.map((row, i) =>
                    <tr className={classNames({'selected': selectedRows[i]})} onClick={() => this.setRowSelected(i, !selectedRows[i])} key={i}>
                      {columns.map((col, j) => {
                        let value = null
                        if (col.cell && isFunction(col.cell)) {
                          value = col.cell(row)
                        } else if (col.key) {
                          if (!value) value = row[col.key]
                        }
                        const cellClass = (col.cellClass && isFunction(col.cellClass)) ? col.cellClass(row) : null;
                        return (
                          <td key={j} className={cellClass}>{value}</td>
                        )
                      })}
                    </tr>
                  )}
                </tbody>
              </table>
            </div>
            {Array.isArray(data) && !this.isStaticData() && (
              <div className={this.props.classes && this.props.classes.pagination}>
                <div className='grid-pagination row'>
                  <div className='col-total col-md-3'>
                    <button disabled={offset + limit >= total} className='btn btn-light btn-sm pl-3 pr-3'
                      onClick={() => this.loadMore()}>
                      <Icon icon={FaRepeat}/> Load more
                    </button>
                    <small className='text-muted ml-2'><b>{data.length} of {total}</b> records loaded</small>
                  </div>
                  {
                    (total > data.length) && (
                      <div className='col-pagination col-md-6'>
                        <ul className='pagination'>
                          {this.renderPagination()}
                        </ul>
                      </div>
                    )
                  }
                  <div className='col-perpage col-md-3'>
                    <select
                      className="form-control per-page"
                      onChange={e => this.handleLimitChange(e.target.value)}
                      value={limit}
                    >
                      {[10, 20, 50, 100, 200, 500].map(pageSize => {
                        return (
                          <option
                            className={(pageSize === limit) ? 'font-weight-bold' : ''}
                            key={pageSize}
                            value={pageSize}
                          >
                            {pageSize} per page
                          </option>
                        )
                      })}
                    </select>
                  </div>
                </div>
              </div>
            )}
          </div>
        </BlockUi>
      </div>
    )
  }

  renderEmptyTable () {
    return (
      <tr>
        <td colSpan={this.props.columns.length} className='text-center p-2'>
          No data found
        </td>
      </tr>
    )
  }
};

Grid.propTypes = {
  id: PropTypes.string.isRequired,
  data: PropTypes.shape({
    data: PropTypes.array,
    total: PropTypes.number,
    loading: PropTypes.bool,
    meta: PropTypes.object, // help information
  }),
  fetch: PropTypes.func,
  columns: PropTypes.arrayOf(PropTypes.shape({
    header: PropTypes.node.isRequired,
    key: PropTypes.string.isRequired,
    sort: PropTypes.bool,
    sum: PropTypes.bool,
    cell: PropTypes.func,
    hide: PropTypes.bool,
    rowClass: PropTypes.func,
    cellClass: PropTypes.func
  })).isRequired,
  filters: PropTypes.arrayOf(PropTypes.shape({
    key: PropTypes.string.isRequired,
    placeholder: PropTypes.string.isRequired,
    options: PropTypes.array,
    type: PropTypes.string,
    value: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number
    ]),
  })),
  classes: PropTypes.shape({
    table: PropTypes.string,
    filters: PropTypes.string,
    pagination: PropTypes.string
  }),
  customs: PropTypes.shape({
    search: PropTypes.node,
  })
}

export default Grid
